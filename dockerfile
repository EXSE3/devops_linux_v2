# Étape 1 : Construire l'application
FROM node:18 AS build

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers de package et installer les dépendances
COPY package*.json  ./
RUN npm install

# Copier le reste des fichiers de l'application
COPY . .

# Construire l'application pour la production
RUN npm run build

# Étape 2 : Servir l'application construite
FROM nginx:alpine

# Copier le résultat de la construction dans le répertoire de nginx
COPY --from=build /app/build /usr/share/nginx/html

# Exposer le port sur lequel nginx sera accessible
EXPOSE 80

# Commande par défaut pour exécuter nginx
CMD ["nginx", "-g", "daemon off;"]


